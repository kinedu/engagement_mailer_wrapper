EngagementMailerWrapper.configure do |config|
  config.api_key = ENV['ENGAGEMENT_API_KEY']
  config.aws_url = ENV['AWS_LAMBDA_URL']
  config.aws_stage = ENV["AWS_ENVIRONMENT"]
  config.aws_customer_alias = ENV['AWS_CUSTOMER_ALIAS']
end