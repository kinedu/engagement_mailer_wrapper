require "thor"
require "engagement_mailer_wrapper/version"
require "engagement_mailer_wrapper/configuration"
require "engagement_mailer_wrapper/service"

module EngagementMailerWrapper
  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.configure
    yield configuration
  end

  class Cli < ::Thor
    include Thor::Actions

    def self.source_root
      File.expand_path('templates', __dir__)
    end

    desc 'initializer:create', 'Create initializer file.'

    def create_initializer
      if yes?("do you want to create initializer file?")
        copy_file(
          'initializer.rb',
          'config/initializers/engagement_mailer_wrapper.rb'
        )
      end
    end
  end
end
