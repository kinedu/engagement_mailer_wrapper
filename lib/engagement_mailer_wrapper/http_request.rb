require 'net/http'

class HttpRequest
  attr_accessor :uri, :http, :request, :params

  def initialize(params)
    @uri = build_uri
    @http = ::Net::HTTP.new(uri.host, uri.port)
    @request = ::Net::HTTP::Post.new(uri.path, headers)

    request.body = params.to_json
    yield self if block_given?
  end

  def start
    http.use_ssl = true
    http.start{ |protocol| protocol.request(request) }
  end

  private

  def build_uri
    URI("#{configurations.aws_url}/client/send_email")
  end

  def headers
    { 'X-API-KEY' => configurations.api_key }
  end

  def configurations
    EngagementMailerWrapper.configuration
  end
end