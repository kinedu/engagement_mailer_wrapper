require_relative 'http_request'

module EngagementMailerWrapper
  class Service
    class << self
      attr_reader :params, :response, :unincluded_params

      # => Run the service if it has
      # all the necessary data
      #
      def call(params)
        @params = params
        unincluded_error unless required_params?
        
        HttpRequest.new(request_params) do |request|
          @response = request.start
        end

        self
      end

      def successfully?
        response.code.start_with?('2')
      end

      private

      # => Collects information from
      # missing data in parameters and
      # response true/false if any
      #
      def required_params?
        @unincluded_params = []

        required_params.each do |param|
          unless params.include?(param)
            unincluded_params << param
          end
        end

        unincluded_params.empty?
      end

      # => *
      def required_params
        %i(locale user_id file_key email subject variables)
      end

      # => *
      def unincluded_error
        raise Exception, "required params #{unincluded_params}"
      end

      # => Unifies configuration parameters
      # and client parameters
      #
      def request_params
        params.merge!(configuration_params)
      end

      # => *
      def configuration_params
        {
          stage: configurations.aws_stage,
          customer_alias: configurations.aws_customer_alias
        }
      end

      # => *
      def configurations
        EngagementMailerWrapper.configuration
      end
    end
  end
end