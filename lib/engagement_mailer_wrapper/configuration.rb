module EngagementMailerWrapper
  class Configuration
    attr_accessor :api_key, :aws_url, :aws_stage, :aws_customer_alias
  end
end