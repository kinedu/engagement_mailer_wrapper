# EngagementMailerWrapper

API wrapper for send email info to kinedu_engagement_api.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'engagement_mailer_wrapper'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install engagement_mailer_wrapper

## Usage

```ruby

# => Configure the service
#
EngagementMailerWrapper.configure do |config|
  config.api_key = ENV['ENGAGEMENT_API_KEY']
  config.aws_url = ENV['AWS_LAMBDA_URL']
  config.aws_stage = ENV["AWS_ENVIRONMENT"]
  config.aws_customer_alias = ENV['AWS_CUSTOMER_ALIAS']
end

# => Prepare parameters
#
params = {
  locale: 'en',
  user_id: 12,
  file_key: 'http://my-file-location.com',
  email: 'armando@example.com',
  subject: 'Example: Welcome to kinedu',
  variables: {
    full_name: "full name"
  }
}

# => Run the service
#
service = EngagementMailerWrapper::Service.call(params)

# => check the service status
#
service.successfully?
service.response

```

To see the parameters that were sent with the request

```ruby

service.params

```

### Service Errors

If the service is executed without some of the required parameters it will return an exception with the parameters that need to be added

```ruby 

# => Example
#
EngagementMailerWrapper::Service.call email: "armando@example.com"

# Will return
#
# Exception (required params [:locale, :user_id, :file_key, :subject, :variables])

```

### Request Errors

To check for errors in the request

```ruby

# => check the service status
#
service.successfully?

# If false verify the response
service.response
service.response.code
service.response.message
service.response.body
```

## Be Happy!
